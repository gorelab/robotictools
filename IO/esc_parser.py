"""
Module parsers .esc files
"""
import re
from numpy import arange

def load_workspace(filename, max_grid_position=30):
    """
    Loads the workspace from a .esc file.

    Information
    -------------

    The format of the esc file is as follows:

    The first block contains contains meta information about the creation of the script.
    The second block contains description of the workspace.
        --{ CFG }--
        CFG = configuration

        First line of CFG describes the carrier IDs located in each grid position.
        To parse it properly, one must know what each ID corresponds to.
        We don't need this for the parser though, so this line is ignored.

        998; 0; # Indicates that the grid position is empty

        # Description of carrier
        # 998; number of sites; [object type]; [object type];
        # 998; ; [object name]; [object name];
        # The number of object types and names must match the number of sites
        # Leave type and name blank if carrier has nothing in that position.

        # Example:
        998; 3; ; Trough 100 ml; Trough 100m ml;
        998; ; Trough A; Trough B;

        # Describes a carrier with 3 sites. The first site is empty.
        # The second site contains a 100 ml trough called "Trough A"

    The third block contains the actual srcipt.
        --{ RPG }--

    Unclear parts of {CFG} section:
        The 3 lines in the beginning.
        The first 998;0; is that for grid position 0?


    Parameters
    ----------
    filename: str
        string, name of .esc file to load hardware model
    max_grid_position : int
        The maximum grid position permitted on this workspace

    Returns
    -----------
    dict : with 'meta', 'carrier_configuration', 'labware_configuration'

    labware_configuration is a list of 2 tuples,
        tuple = (carrier ID, grid position)

    TODO UPDATE
    """
    meta = {} # Dictionary to hold meta data information
    labware_configuration = [] # list of dict object descriptions

    # read file
    with open(filename, 'r') as f:
        lines = f.readlines()

    # Unsure if header is of constant length. Find first line of the specification
    for line in range(len(lines)):
        if lines[line].startswith('998'):
            first_grid_line = line
            break

    # Read meta_data
    meta['workspace ID'] = lines[0].strip()
    meta['Last-modified date']  = lines[1].split()[0].split('_')[0]
    meta['Last-modified time']  = lines[1].split()[0].split('_')[1]
    meta['Created by']  = lines[1].split()[1]

    # Generate workspace model

    ## Read carrier positions
    carrier_configuration = map(int, lines[first_grid_line-1].split(';')[2:-1])
    carrier_configuration = zip(carrier_configuration, arange(len(carrier_configuration))+1)
    carrier_configuration = filter(lambda x : x[0] >= 0, carrier_configuration)
    carrier_configuration = [{'ID' : x[0], 'grid' : x[1]} for x in carrier_configuration]

    ### IMPORTANT: num_sites for carrier configuration is updated below!!

    ## Read labware positions
    cfg_str = filter(lambda x : x.startswith('998'), lines)
    grid, line, carrier_num = 0, 0, 0

    while grid < max_grid_position:
        # we don't need the last separator
        line_content = cfg_str[line].strip().split(';')[:-1]
        nSites = int(line_content[1])

        if nSites > 0:
            labware_types = line_content[2:] # Labware types
            labware_names = cfg_str[line + 1].strip().split(';')[1:] # Labware names
            for site in range(nSites):
                if labware_types[site] != '':
                    conf = {
                                'name' : labware_names[site],
                                'labware_type' : labware_types[site],
                                'grid' : grid,
                                'carrier_num' : carrier_num,
                                'carrier_site' : site + 1,
                            }
                    labware_configuration.append(conf)
                else:
                    # Means that there is no labware at this carrier site
                    pass
            carrier_configuration[carrier_num]['num_sites'] = nSites
            carrier_num += 1
            line += 1 # Increase by 1 because following line describes name of labware
        line += 1
        grid += 1

    workspace_configuration = dict(meta=meta,
            carrier_configuration=carrier_configuration,
            labware_configuration=labware_configuration)
    return workspace_configuration

def get_till_RPG_section(filename):
    """
    Returns the part of the file until the { RPG } section.
    """
    with open(filename, 'r') as f:
        lines = f.readlines()
        RPG_section_start_line = 0
        for num, l in enumerate(lines):
            if '{ RPG }' in l:
                RPG_section_start_line = num
        return ''.join(lines[:(RPG_section_start_line+1)])

if __name__ == '__main__':
    #configuration = load_workspace('../tests/workspaces/workspace_01.esc')
    configuration = get_CFG_section('../tests/workspaces/workspace_01.esc')
    print configuration
    #build_workspace_objects(configuration)


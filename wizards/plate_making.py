"""
Contains a set of functions to help making plates.
"""

def dilutePlate(robot, source, destination, dilutionFactor, types=None):
    """
    source, destination:    Plate objects, 
                            or PlateHolder objects, 
                            or string, name of labware
    dilutionFactor: dilution factor (<1)
    types: type of tip.

    Returns: None

    Dilutes source plate by dilutionFactor, adjusting volumes based on the current volumes in destination.
    """

    source = robot._normalize('plate', source)
    destination = robot._normalize('plate', destination)

    if source.types != destination.types:
        print 'Incompatible types: ', source.types, ' and ', destination.types
        return

    destVolumes = destination.getVolumes()
    for col in range(source.xlen):
        volumes = destVolumes[col,:]
        aspirateVols = list(volumes * dilutionFactor/(1-dilutionFactor))

        robot.getDiti(types=types)
        robot.aspirate(source, col+1, aspirateVols)
        robot.dispense(destination, col+1, aspirateVols)
        robot.dropDiti()

def fillPlate(robot, source, destination, volume, types=None):
    """
    source, destination:    Plate objects,
                            or PlateHolder objects,
                            or string, name of labware
    volume: float, volume to transfer,
            or list of floats, volumes to transfer in each row/column (depending on list size)
            or numpy array of floats, volumes to transfer in each well.
    types: type of tip.

    Returns: None

    Transfers volume from every well in source to destination.
    """
    source = robot._normalize('plate', source)
    destination = robot._normalize('plate', destination)
    volume = robot._normalize('platemap', volume, shape=(source.xlen, source.ylen))

    if source.types != destination.types:
        print 'Incompatible types: ', source.types, ' and ', destination.types
        return

    for col in range(source.xlen):
        robot.getDiti(tips='all',types=types)
        robot.aspirate(source, col+1, volume[col], [1,2,3,4,5,6,7,8], 'Water Free Dispense')
        robot.dispense(destination, col+1, volume[col], [1,2,3,4,5,6,7,8], 'Water Free Dispense')
        robot.dropDiti()    

def makePlate(robot, destination, trough, volumes, types = None, setback=False, reverseorder=False):
    """
    destination:    Plate object, 
                    or PlateHolder object, 
                    or string, name of labware
                    where to put the plate while filling the plates.
    hotel:  Hotel object
    trough: Trough objects
    volume:     float, how much to put in each well, 
                or numpy array (matching plate shape)
    types: type of tip.

    Returns: None

    Fills numPlates plates from hotel with the specified volumes from troughs.
    TODO: Add normalization of trough names.
    """
    #normalize plates and volumes
    destination = robot._normalize('plate', destination)
    vols = robot._normalize('platemap', volumes, shape=(destination.xlen, destination.ylen))
    robot.getDiti(types=types)
    cols = range(len(vols))
    if reverseorder == True:
        cols.reverse()
    for column in cols:
        robot.aspirateTrough(trough, vols[column])
        robot.dispense(destination, column+1, vols[column])
    if not setback:
        robot.dropDiti()
    else:
        robot.setDiti()

def makePlates(robot, numPlates, hotel, destination, lid, troughs, volumes, types=None, setback=False):
    """
    destination:    Plate object,
                    or PlateHolder object,
                    or string, name of labware
                    where to put the plate while filling the plates.
    hotel:  Hotel object
    troughs: list of Trough objects
    volume: list, same size as troughs, containing some combination of:
                float, how much to put in each well,
                or numpy array (matching plate shape)
    types: type of tip.

    Returns: None

    Fills numPlates plates from hotel with the specified volumes from troughs.
    TODO: Add normalization of trough names.
    """
    #normalize plates and volumes
    destination = robot._normalize('plateholder', destination)
    vols = []

    plates = []

    for i in range(numPlates):
            plates.append(Plate(robot.hotel.slots[i], Plate.P96_Well_Microplate_F3072))

        if isinstance(volumes, float) or isinstance(volumes, int):
            volumes = [volumes]
        for i in volumes:
            vols.append(robot._normalize('platemap', i, shape=(destination.xlen, destination.ylen)))

        for plate in range(numPlates):
            robot.transferLabware(plates[plate], destination, lid=lid, uncover=True)
            for reagent in range(len(troughs)):
                robot.makePlate(destination, troughs[reagent], vols[reagent], setback=setback)
            robot.transferLabware(plates[plate], hotel.slots[plate], lid=lid, uncover=False)


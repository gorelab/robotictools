"""
TODO
Separate devices from carriers
Change site to site number...
"""
from sites import LabwareSite


class LabwareCarrier(object):
    """
    Base class for carriers.
    Anything that can hold labware should derive from this class.
    """
    def __init__(self, num_sites, carrier_type=None, allowed_labware=None):
        self.num_sites = num_sites if num_sites is not None else 0
        self.carrier_type = carrier_type

        if num_sites > 0:
            sites = [LabwareSite(i+1, self, allowed_labware=allowed_labware) for i in range(self.num_sites)]
            self.child_sites = sites
        else:
            self.child_sites = None

    @property
    def child_labware(self):
        if self.child_sites is not None:
            return [site.labware for site in self.child_sites if site.labware is not None]
        else:
            return None

    def find_labware(self, criteria=None, output_type='list', **filter_kwargs):
        """
        Returns a list of labware matching ALL the criteria and filtering_keywords.

        Parameters
        ------------
        criteria : [None | callable]
            None : Any/all labware will be returned.
            As a callable will be given the labware and must return True or False.
        output_type : ['list' | 'tree']
            if 'tree', then the output preserves the structure of which labware
            carriers which other labware.
        filter_kwargs :
            Use as attr=value pairs

        Returns
        ----------
        list or tree

        Examples
        -----------
        x.find_labware(name='Trough')
        x.find_labware(labware_type='MP 3Pos') # Returns a list of MP 3 Pos carriers if found.
        x.find_labware(category='Plate')
        """
        matching_lawbare = []

        if output_type not in ('list', 'tree'):
            msg = "output_type must be 'list' or 'tree'"
            raise ValueError(msg)

        pass_filter = True

        if (criteria is not None):
            if (not hasattr(criteria, '__call__')):
                raise TypeError('criteria must be a function')
            elif not criteria(self):
                pass_filter = False

        # Check filter_kwargs
        for attr, value in filter_kwargs.items():
            if not hasattr(self, attr):
                pass_filter = False
                break
            elif getattr(self, attr) != value:
                pass_filter = False
                break

        if pass_filter:
            if output_type == 'list':
                matching_lawbare.append(self)
            elif output_type == 'tree':
                matching_lawbare.append(self)

        if self.child_labware is not None:
            for child in self.child_labware:
                child_output = child.find_labware(criteria,
                        output_type=output_type, **filter_kwargs)

                if output_type == 'list':
                    matching_lawbare.extend(child_output)
                elif output_type == 'tree':
                    matching_lawbare.append(child_output)

        return matching_lawbare

    def print_labware_tree(self, print_site_numbers=True):
        """
        Print's a tree of child lawbare.

        TODO: rewrite this to actually generate a tree so we can use filtering functions..
        """
        labware_tree = self.find_labware(output_type='tree')
        print_tree(labware_tree, 0)

    def print_summary(self):
        output = ''
        for site in self.child_sites:
            output += '\n\t{}'.format(site.short_description())
        print output

    def __getitem__(self, key):
        """
        key : int (starts from 1 not 0) THIS MAY CHANGE LATER ON.
        TODO: Decide whether we count from 0 or from 1.
        """
        if key == 0:
            raise IndexError
        return self.child_sites[key-1]

    def __str__(self):
        return 'Name: {}, Type: {}, Grid: {}'.format(self.name, self.carrier_type, self.grid)

    def __repr__(self):
        return self.__str__()


###
# Helper functions
def print_tree(tree, level=0):
    output = ''
    if len(tree) == 0:
        return

    for node in tree:
        if isinstance(node, list):
            output += format(print_tree(node, level+1))
        else:
            node_str = str(node)
            output += '\n' + '\t' * level + '* ' + node_str
    if level == 0:
        print output
    else:
        return output

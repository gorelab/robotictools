from carriers import LabwareCarrier
from RobotTools.IO import esc_parser
from labware import Labware, Tipbox, Plate, create_labware #, Trough, Plate

class Workspace(LabwareCarrier):
    """
    Defines the workspace for the robot.
    Keeps track of all the lawbare and carriers.
    """
    def __init__(self, grid_size=30):
        print 'allowed_labware for workspace is hardcoded.'
        LabwareCarrier.__init__(self, grid_size, allowed_labware='all')
        self.name = 'workspace'
        for site_num, site in enumerate(self.child_sites):
            site._coordinates = site_num + 1 # Start to count from 1 for grid positions

        self._add_hotel()

    def __str__(self):
        return self.name

    def _add_hotel(self):
        print 'Addition of hotel is hardcoded'
        from sites import LabwareSite
        hotel_site = LabwareSite(100, self, allowed_labware='all', coordinates=4)
        self.child_sites.append(hotel_site)
        hotel = create_labware(labware_type='16 Position Hotel 30034177')
        hotel_site.place_labware(hotel)

    def remove_labware(self, labware):
        labware.remove()

    @classmethod
    def from_esc_file(cls, filename, carrier_ID_to_type_map=None):
        """
        Loads a workspace configuration from an .esc file.

        Parameters
        ------------
        filename : str
            name of .esc file to load hardware model

        TODO: Refactor, some of this code should migrate to __init__ and some to a method.
        """
        workspace = cls()
        workspace._original_esc = filename # HACKISH

        workspace_configuration = esc_parser.load_workspace(filename)
        carrier_configuration = workspace_configuration['carrier_configuration']
        labware_configuration = workspace_configuration['labware_configuration']

        # Set up carriers
        carrier_list = []

        ##
        # The code below is split to two parts.
        # The two part design is not so good, but it is based on the way EVOwares esc files are designed.
        # The first part sets up labware on the first level of the hierarchy, which evoware refers to
        # as carriers.
        # The second part sets up the labware on the second level of hierarchy, which evoware refers
        # to as labware. These labware are located on the carriers from level one.

        for cfg in carrier_configuration:
            ID = cfg.pop('ID')
            grid = cfg.pop('grid')
            carrier = create_labware(ID=ID)
            carrier_list.append(carrier)
            workspace[grid].place_labware(carrier)

        # Set up labware and place it on carriers
        labware_list = []
        for cfg in labware_configuration:
            labware_site = carrier_list[cfg['carrier_num']][cfg['carrier_site']]
            labware_type = cfg['labware_type']
            labware = create_labware(labware_type=labware_type, name=cfg['name'])
            labware_site.place_labware(labware)
            labware_list.append(labware)
        return workspace

    def to_esc_file(self, output_file):
        print('WARNING This method simply uses copies the configuration from\
        the original esc file it does not update the configuration based on any\
        changes that have happend in the workspace.')
        ### ATTENTION: If you're trying to implement this function make sure
        # that you figure out how to distinguish between intiial configuration
        # and current configuration. This is especially relevant if you're using
        # the interface to generate a script... For example, in this case
        # the configuration can change if you're moving labware using one of the arms.
        if not hasattr(self, '_original_esc'):
            raise Exception("workspace must have been loaded from an esc file..  function not fully implemented.")
        else:
            CFG_section = esc_parser.get_till_RPG_section(self._original_esc)
            with open(output_file, 'w') as f:
                f.write(CFG_section)

if __name__ == '__main__':
    w = Workspace.from_esc_file('../tests/workspaces/workspace_01.esc')
    print w.child_labware


"""
Module contains description of labware and some configuration.
"""
from carriers import LabwareCarrier

class Labware(LabwareCarrier):
    """
    Base class for labware.
    """
    def __init__(self, labware_type='Generic', name='Unnamed', labware_sites=None, pipettor_sites=None,
            allowed_labware=None, **labware_properties):

        LabwareCarrier.__init__(self, num_sites=labware_sites, carrier_type=labware_type,
                allowed_labware=allowed_labware)

        self.name = name
        self.category = labware_properties.pop('category', 'unknown')
        self.movable = labware_properties.pop('movable', False)
        self.properties = labware_properties
        self.labware_type = labware_type

        if pipettor_sites not in (None, 0):
            self.shape = pipettor_sites
        else:
            self.shape = None

        self.site = None # Means stands directly on robot

        self._initialize() # Called by derived classes for further initialization

    def _initialize(self):
        """
        Used by derived classes for further initilization.
        """
        pass

    def short_description(self):
        """ Returns just type and name """
        return '{} ({})'.format(self.labware_type, self.name)

    def __str__(self):
        return self.short_description()

    @property
    def coordinates(self):
        return self.site.coordinates

    @property
    def grid(self):
        """ Finds grid position on robot where labware is located. """
        return self.coordinates

    @property
    def carrier(self):
        """ Returns the current carrier of the labware. """
        return self.site.carrier

    def remove(self):
        """ Removes the labware from the workspace.
        This probably requires a bit more thought.
        """
        self.site.labware = None

    ##
    # Should refactor
    @property
    def xlen(self):
        return self.shape[1]

    @property
    def ylen(self):
        return self.shape[1]


class Tipbox(Labware):
    """
    Implement tip refill and remove options.
    """
    pass

class Plate(Labware):
    @property
    def covered(self):
        return len(self.child_labware) == 1 # True if covered with lid

    @property
    def lid_site(self):
        return self[1]

    def create_lid(self):
        lid_type = self.lid_site.allowed_labware[0]
        lid = create_labware(labware_type=lid_type)
        self.lid_site.place_labware(lid)

    @property
    def current_lid(self):
        """
        Only returns a lid if its on the plate.
        """
        return self.lid_site.labware

from RobotTools import database
from ast import literal_eval # Safe way to evaluate python expressions

def create_labware(ID=None, labware_type=None, **other_meta):
    """
    Chooses which kind of labware to create based on provided meta information.
    TODO: Refactor with database lookup...
    """
    lm = other_meta

    if ID is not None:
        meta = database.get_by_ID(ID)
    if labware_type is not None:
        meta = database.get_by_labware_type(labware_type)

    lm.update(meta)

    for key in ['pipettor_sites', 'allowed_labware']:
        lm[key] = literal_eval(lm[key])

    _class_factory = {'Tipbox' : Tipbox, 'Plate' : Plate}
    _class = _class_factory.get(lm['generic_type'], Labware)
    labware = _class(**lm)
    return labware

#
#class Plate(Labware):
    #"""
    #"""
    #types = None
    #xlen = 12
    #ylen = 8
    #wells = None
    #holder = None
    #lidHolder = None
    #covered = True
    #maxcapacity = 300
#
    #def __init__(self, labware_type, name='Unnamed', category='plate', num_sites=1, allowed_labware=None):
#
#
    #def __init__(self, *args, **kwargs):
        #"""
        #Parameters:
        #holder: PlateHolder object, where the plate starts
        #types: string, type of labware, default plateholder value
        #"""
        #self.wells = []
        #self.holder = holder
        #self.types = holder.types
        #if not types == None:
            #self.types = types
            #self.holder.types = types
        #self.holder.occupied = self
        #for i in range(self.xlen):
            #wellx = []
            #for j in range(self.ylen):
                #wellx.append(Well(i,j,self, filledVolume=initialVolume))
            #self.wells.append(wellx)
        #self.xlen = lwdict[self.types][0]
        #self.ylen = lwdict[self.types][1]

    #def get_volumes(self):
        #"""
        #col: column of plate
#
        #Returns: numpy array
#
        #Returns an array of the current volumes in column col of the plate.
        #"""
        #vols = []
        #for col in range(self.xlen):
            #column = []
            #for well in self.wells[col]:
                #column.append(well.filledVolume)
            #vols.append(column)
        #return numpy.array(vols)

"""
A device is any object on the robot that can do something.
Foe example, a plate reader can measure a plate, a pipettor arm can aspirate.

A device API must offer the following:

* Methods that correspond to the action the device can do, as documented as
possible and as generic as possible.

* For each action, the device should use a 'driver' to generate the corresponding
driver command. (e.g., the actually measure something the plate reader may need
to send a command via the serial port.)

* The device must let know it's observers when 'driver' commands are issued.

The idea is to separate the logic that deals with keeping track of labware
and tips apart from the actual driver commands.

The advantage is that the interface becomes somewhat agnostic to the actual
hardware as long as the drivers are implemented correctly.

The disadvantage is the added complexity, especially since the
actual implementation below is not the best.

(For example, it feels very repetitive, and I wasn't sure whether in general
drivers should be implemented as classes or just as modules with functions
defined inside of them.) - EY
"""

from RobotTools.core.carriers import LabwareCarrier
from RobotTools.core.notification import Observable
from RobotTools.core.doc import doc_replacer
from RobotTools.core.util import generate_site_selection
import numpy

class Device(Observable):
    """
    All devices should inherit from this class.
    """
    def __init__(self, name, driver, ID=None):
        Observable.__init__(self)
        self.name = name
        self.ID = ID
        self._driver = driver

    def _issue_command(self, command_name, **kwargs):
        if 'self' in kwargs:
            kwargs.pop('self')
        kwargs['device'] = self

        driver_command = getattr(self._driver, command_name)
        command = driver_command(**kwargs)

        # _notify observer of the driver action
        self._notify(command=command)

class Mover(Device):
    """
    Device used to move labware around.
    """
    def __init__(self, name, driver, ID):
        Device.__init__(self, name, driver, ID)

    def transfer(self, labware, destination_site, **command_kwargs):
        """
        Transfer Labware

        Parameters
        ----------
        labware: Labware
            Which labware to move
        destination_site: LabwareSite
        command_kwargs : other keyword arguments to be passed to the command.
        """
        destination_site.check_availability(labware)

        self._issue_command('transfer', labware=labware,
                destination_site=destination_site, **command_kwargs)

        destination_site.place_labware(labware)


class Pipettor(Device):
    """
    Device used to move liquids around.
    """
    def __init__(self, name, driver, ID, pipettor_shape, tip_counting_order='row'):
        """
        Parameters
        ----------
        name : str
            Name of the device
        driver : Driver
            knows how to generate the specific commands
        ID : anything
            passed to the drivers in case the drivers require an arm ID
        shape : tuple
            Defines the shape of the pipettor. For example, the LiHa
            is (rows, cols) = (8, 1) = (y direction, x direction).
        """
        Device.__init__(self, name, driver, ID)
        self.pipettor_shape =  pipettor_shape # For example, (rows, cols)=(8, 1) for liha.
        self.liquid_profile = None
        # with 'row' the 2nd tip is 'B1', but with 'col' the second tip is 'A2'.
        self.tip_counting_order = tip_counting_order
        self.num_channels = numpy.product(pipettor_shape) # total number of channels
        # Used to store channel information
        self.channels = numpy.empty(pipettor_shape, dtype=object)

    @doc_replacer
    def aspirate(self, tips, volumes, labware, wells, well_step=1, increment_order='row', liquid_class='default'):
        """
        Aspirates liquids of a certain volume from a given labwares' wells.

        Parameters
        -------------
        {_tip_formatting_options}
        {_volume_formatting_options}
        {_site_selection_options}
        labware : Labware object
        """
        kwargs = locals()
        kwargs.pop('self')
        #if liquid_class == 'default':
            #kwargs['liquid_class'] = self.liquid_profile['aspirate']
        kwargs['tips'] = self._format_tips_as_tip_list(tips)
        self._issue_command('aspirate', **kwargs)

    @doc_replacer
    def pick_up_tips(self, tips, labware, wells, well_step=1, increment_order='row'):
        """
        Issues pick_up_diti command.

        Parameters
        -------------
        {_tip_formatting_options}
        {_site_selection_options}
        labware : Labware object
        """
        ##
        kwargs = locals()
        kwargs.pop('self')
        ##
        #kwargs['tips'] = self._format_tips_as_tip_list(tips)
        #tips = kwargs['tips']
        #tip_index = self._tip_loc_to_index(tips)
        #print tip_index
        #print self.channels[tip_index]
        #if self.channels[tip_index]:
            #raise Exception('Tips already mounted on some of the channels of the pipettor')
        #else:
            #self.channels[tip_index] = 1
        kwargs['tips'] = self._format_tips_as_tip_list(tips)
        self._issue_command('pick_up_tips', **kwargs)

    @doc_replacer
    def pick_up_tips_automatically(self, tips, tip_type):
        """
        Automatically gets the tips.

        Parameters
        -------------
        {_tip_formatting_options}
        tip_type : the labware_type of the tipbox
        """
        ##
        kwargs = locals()
        kwargs.pop('self')
        ##
        kwargs['tips'] = self._format_tips_as_tip_list(tips)
        self._issue_command('pick_up_tips_automatically', **kwargs)

    @doc_replacer
    def put_down_tips(self, tips, labware, wells, well_step=1, increment_order='row'):
        """
        Issues put_down_diti command.

        Parameters
        -------------
        {_tip_formatting_options}
        {_site_selection_options}
        labware : Labware object
        """
        kwargs = locals()
        kwargs.pop('self')
        kwargs['tips'] = self._format_tips_as_tip_list(tips)
        self._issue_command('put_down_tips', **kwargs)

    @doc_replacer
    def drop_tips(self, tips, labware, airgap=10, airgap_speed=70, **kwargs):
        """
        Issues drop_diti command.
        TODO: Remove labware here? Is this different from put_down_tips?

        Parameters
        -------------
        {_tip_formatting_options}
        labware : Labware object
        """
        kwargs = locals()
        kwargs.pop('self')
        kwargs['tips'] = self._format_tips_as_tip_list(tips)
        self._issue_command('drop_tips', **kwargs)


    @doc_replacer
    def dispense(self, tips, volumes, labware, wells, well_step=1, increment_order='row', liquid_class='default'):
        """
        Issues dispense command.

        Parameters
        -------------
        {_tip_formatting_options}
        {_volume_formatting_options}
        {_site_selection_options}
        labware : Labware object
        """
        kwargs = locals()
        kwargs.pop('self')
        kwargs['tips'] = self._format_tips_as_tip_list(tips)
        self._issue_command('dispense', **kwargs)

    @doc_replacer
    def mix(self, tips, volumes, labware, wells, well_step=1, increment_order='row', liquid_class='default', mixing_cycles=3):
        """
        Issues mix command.

        Parameters
        -------------
        {_tip_formatting_options}
        {_volume_formatting_options}
        {_site_selection_options}
        labware : Labware object
        """
        kwargs = locals()
        kwargs.pop('self')
        kwargs['tips'] = self._format_tips_as_tip_list(tips)
        self._issue_command('mix', **kwargs)

    @doc_replacer
    def _format_tips_as_tip_list(self, tips):
        """
        Given tips in allowed formats returns the valid tips as a list.

        Parameters
        ----------
        {_tip_formatting_options}

        Returns
        -------
        tip_matrix
            if tips was [1, 2, 3]
            tip_matrix should return
            [[0, 1, 2], [0, 0, 0]] # For a pipettor of shape (8, 1)
        """
        if isinstance(tips, str):
            if tips == 'all':
                tips_modified = 'all'
            else:
                if len(tips) != self.num_channels:
                    raise ValueError('tips must be of length {}'.format(self.num_channels))
                tips_modified = []
                for tip_num in range(self.num_channels):
                    if tips[tip_num] == '1':
                        tips_modified.append(tip_num+1) # Counting tips starting from 1
        elif isinstance(tips, list):
            tips_modified = tips
        elif isinstance(tips, int):
            tips_modified = [tips]
        else:
            # Update error message later
            raise TypeError('Tips={} is of wrong type'.format(tips))
        return generate_site_selection(tips_modified, self.pipettor_shape, num_sites='not needed in this case',
                increment_order=self.tip_counting_order, input_type='position')

from RobotTools.core.labware import Labware

class PlateReader(Labware, Device):
    """
    Implements generic interface for plate readers.
    """
    def __init__(self, name, driver, ID, allowed_labware=None):
        Device.__init__(self, name, driver, ID)
	Labware.__init__(self, name, allowed_labware='all', labware_sites=1)

    def open(self, **kwargs):
        """
        Opens Plate Reader Doors / Releases Plate (if needed)
        """
        kwargs = locals()
        kwargs.pop('self')
        self._issue_command('open', **kwargs)

    def close(self, **kwargs):
        """
        Closes Plate Reader Doors / Inserts Plates (if needed)
        """
        kwargs = locals()
        kwargs.pop('self')
        self._issue_command('close', **kwargs)

    def measure(self, output_path, wavelengths=None, shake_duration=0, shake_settle_time=0, **kwargs):
        """
        Measured the plate
        output_path
        """
        kwargs = locals()
        kwargs.pop('self')
        self._issue_command('measure', **kwargs)

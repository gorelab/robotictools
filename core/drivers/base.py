"""
A generic driver than can be used for debugging/implementation of drivers.
"""
class Driver(object):
    def __init__(self, device=None, verbose=True):
        self.device = device
        self.verbose = verbose

    def __getattr__(self, key):
        def virtual_method(*args, **kwargs):
            output = 'Device: {}, Driver: {}, Using Virtual Method:' +\
                    '{}, with args {}, kwargs{}'
            output = output.format(self.device, self, key, args, kwargs)
            if self.verbose:
                print output
            return output
        return virtual_method

if __name__ == '__main__':
    driver = Driver('virtual device')
    driver.add_virtual_method(['do_something'])
    print driver.do_something(0, 1, a=2, b=3)

"""
Description of actions that the RomaArm can do.
"""

def transfer(device, labware, destination_site, lid=None, lid_destination_site=None, **command_kwargs):
    """
    Transfer Labware

    Parameters
    ----------
    labware: Labware
        Which labware to move
    destination_site: LabwareSite
    """
    return_home = command_kwargs.pop('return_home', True)
    vector_name = command_kwargs.pop('vector_name', "Narrow")

    # Lid handling
    handle_lid = 0
    lid_grid = 0
    lid_site = '(Not defined)'
    lid_carrier_type = ''
    lid_handling_time = 0 # Doesn't do anything now

    if lid is not None:
        # Cover at destination
        handle_lid = 1
        lid_grid = lid.grid
        lid_site = lid.site.site_number
        lid_handling_time = 0 # Cover at destination
        lid_carrier_type = lid.site.carrier.carrier_type

    if lid_destination_site is not None:
        # This corresponds to uncover at source
        # Uncover at source
        handle_lid = 1
        lid_grid = lid_destination_site.grid
        lid_site = lid_destination_site.site_number
        lid_handling_time = 1 # Uncover at source
        lid_carrier_type = lid_destination_site.carrier.carrier_type

    command_pars = dict(vector_name=vector_name,
        speed=1, # Slow speed. Use 0 for max but not recommended.,
        arm_number=device.ID,
        labware_grid=labware.grid,
        labware_site=labware.site.site_number,
        labware_type=labware.labware_type,
        # Carrier types -- Why is this even needed for EVOware?
        source_carrier_type=labware.carrier.carrier_type,
        destination_carrier_type=destination_site.carrier.carrier_type,
        # Lid handling (disable all temporarily)
        handle_lid=handle_lid,
        lid_site=lid_site,
        lid_carrier_type=lid_carrier_type,
        lid_grid=lid_grid,
        lid_handling_time=lid_handling_time,
        # Destination grid
        destination_grid=destination_site.grid,
        destination_site=destination_site.site_number,
        return_home=int(return_home))

    command_pars.update(**command_kwargs)

    command = 'Transfer_Rack("{labware_grid}","{destination_grid}",' + \
            '{return_home},{handle_lid},{speed},{arm_number},' + \
            '{lid_handling_time},"{lid_grid}","{labware_type}",' + \
            '"{vector_name}","","","{source_carrier_type}",' + \
            '"{lid_carrier_type}","{destination_carrier_type}",' + \
            '"{labware_site}","{lid_site}","{destination_site}");'
    command = command.format(**command_pars)

    return command

#def romaVector(self, name, grid, site, direction=0, back=1, beginaction=2, endaction=2, speed=1, roma=0):
    #"""
    #Move RoMa according to predefined vector
#
    #Parameters
    #----------
    #direction: 0 = from safe to end position
               #1 = from end to safe position
    #back: 0 = remain at final position
          #1 = retrace to where the arm began
    #beginaction: gripper action at safe position:
                 #0 = open
                 #1 = close
                 #2 = do not move
    #endaction: gripper action at end position
    #speed: 0 = maximum
           #1 = slow, as defined in vector
    #"""
    ## output
    #wstr = 'Vector("'+name+'","'+str(grid)+'","'+str(site)+'",'+str(direction)+','+str(back)+','+str(beginaction)
    #wstr += ','+str(endaction)+','+str(speed)+','+str(roma)+');\n'
    #return wstr
#
## Common combinations of functions for convenience
#def removeLid(self, source):
    #"""
    #Picks up the lid from a plate, and moves robotic arm to home position
    #"""
    #if source.occupied == None:
        #self.romaVector('MP 3Pos_Narrow_1', source.grid, source.site, endaction=1)
    #else:
        #self.romaVector('MP 3Pos Narrow Lid', source.grid, source.site, endaction=1)
    #return
#
#def replaceLid(self, destination):
    #"""
    #Puts back the lid the robot is currently holding, and moves robotic arm to home position
    #TODO:validate that it is indeed holding a lid
    #"""
    #if destination.occupied == None:
        #self.romaVector('MP 3Pos_Narrow_1', destination.grid, destination.site, endaction=0)
    #else:
        #self.romaVector('MP 3Pos Narrow Lid', destination.grid, destination.site, endaction=0)
    #return

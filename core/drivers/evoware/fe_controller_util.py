"""
Freedom Evoware controller util file.

Used by freedom_evoware_controller.py
"""
#Constants specific to EVOWARE
class EVOConstants:
    PP_EMERGENCY                  = 1          # from enum tagSC_EmergencyLevel
    PP_HIGHEMERGENCY              = 2          # from enum tagSC_EmergencyLevel
    PP_NORMAL                     = 0          # from enum tagSC_EmergencyLevel
    LAMP_GREEN                    = 1          # from enum tagSC_LampStatus
    LAMP_GREENFLASHING            = 2          # from enum tagSC_LampStatus
    LAMP_OFF                      = 0          # from enum tagSC_LampStatus
    LAMP_REDFLASHING              = 3          # from enum tagSC_LampStatus
    PS_BUSY                       = 1          # from enum tagSC_ProcessStatus
    PS_ERROR                      = 3          # from enum tagSC_ProcessStatus
    PS_FINISHED                   = 2          # from enum tagSC_ProcessStatus
    PS_IDLE                       = 0          # from enum tagSC_ProcessStatus
    PS_STOPPED                    = 4          # from enum tagSC_ProcessStatus
    SS_ABORTED                    = 3          # from enum tagSC_ScriptStatus
    SS_BUSY                       = 2          # from enum tagSC_ScriptStatus
    SS_ERROR                      = 7          # from enum tagSC_ScriptStatus
    SS_IDLE                       = 1          # from enum tagSC_ScriptStatus
    SS_PAUSED                     = 6          # from enum tagSC_ScriptStatus
    SS_PIPETTING                  = 5          # from enum tagSC_ScriptStatus
    SS_SIMULATION                 = 8          # from enum tagSC_ScriptStatus
    SS_STATUS_ERROR               = 9          # from enum tagSC_ScriptStatus
    SS_STOPPED                    = 4          # from enum tagSC_ScriptStatus
    SS_UNKNOWN                    = 0          # from enum tagSC_ScriptStatus
    STATUS_ABORTED                = 262144     # from enum tagSC_Status
    STATUS_BUSY                   = 131072     # from enum tagSC_Status
    STATUS_CONNECTION_ERROR       = 16777216   # from enum tagSC_Status
    STATUS_DEADLOCK               = 4096       # from enum tagSC_Status
    STATUS_ERROR                  = 2097152    # from enum tagSC_Status
    STATUS_EXECUTIONERROR         = 8192       # from enum tagSC_Status
    STATUS_IDLE                   = 65536      # from enum tagSC_Status
    STATUS_INITIALIZED            = 16         # from enum tagSC_Status
    STATUS_INITIALIZING           = 8          # from enum tagSC_Status
    STATUS_LOADING                = 2          # from enum tagSC_Status
    STATUS_LOGON_ERROR            = 8388608    # from enum tagSC_Status
    STATUS_NOINTRUMENTS           = 1          # from enum tagSC_Status
    STATUS_NOTINITIALIZED         = 4          # from enum tagSC_Status
    STATUS_PAUSED                 = 1024       # from enum tagSC_Status
    STATUS_PAUSEREQUESTED         = 512        # from enum tagSC_Status
    STATUS_PIPETTING              = 1048576    # from enum tagSC_Status
    STATUS_RESOURCEMISSING        = 2048       # from enum tagSC_Status
    STATUS_RUNNING                = 256        # from enum tagSC_Status
    STATUS_SHUTDOWN               = 64         # from enum tagSC_Status
    STATUS_SHUTTINGDOWN           = 32         # from enum tagSC_Status
    STATUS_SIMULATION             = 4194304    # from enum tagSC_Status
    STATUS_STOPPED                = 524288     # from enum tagSC_Status
    STATUS_TIMEVIOLATION          = 16384      # from enum tagSC_Status
    STATUS_UNKNOWN                = 0          # from enum tagSC_Status
    STATUS_UNLOADING              = 128        # from enum tagSC_Status
    TIP_ACTIVE                    = 4          # from enum tagSC_TipType
    TIP_DITI                      = 1          # from enum tagSC_TipType
    TIP_DITILOVOL                 = 3          # from enum tagSC_TipType
    TIP_FIXLOVOL                  = 2          # from enum tagSC_TipType
    TIP_STANDARD                  = 0          # from enum tagSC_TipType
    TIP_TEMO384                   = 8          # from enum tagSC_TipType
    TIP_TEMO384IMPULSE            = 7          # from enum tagSC_TipType
    TIP_TEMO96DITI                = 6          # from enum tagSC_TipType
    TIP_TEMOFIXED                 = 5          # from enum tagSC_TipType
    VS_ITERATION                  = 1          # from enum tagSC_VariableScope
    VS_PROCESS                    = 0          # from enum tagSC_VariableScope
    STATUS = {
        262144   : "ABORTED               ",
        131072   : "BUSY                  ",
        16777216 : "CONNECTION_ERROR      ",
        4096     : "DEADLOCK              ",
        2097152  : "ERROR                 ",
        8192     : "EXECUTIONERROR        ",
        65536    : "IDLE                  ",
        16       : "INITIALIZED           ",
        8        : "INITIALIZING          ",
        2        : "LOADING               ",
        8388608  : "LOGON_ERROR           ",
        1        : "NOINTRUMENTS          ",
        4        : "NOTINITIALIZED        ",
        1024     : "PAUSED                ",
        512      : "PAUSEREQUESTED        ",
        1048576  : "PIPETTING             ",
        2048     : "RESOURCEMISSING       ",
        256      : "RUNNING               ",
        64       : "SHUTDOWN              ",
        32       : "SHUTTINGDOWN          ",
        4194304  : "SIMULATION            ",
        524288   : "STOPPED               ",
        16384    : "TIMEVIOLATION         ",
        0        : "UNKNOWN               ",
        128      : "UNLOADING             ",
    }


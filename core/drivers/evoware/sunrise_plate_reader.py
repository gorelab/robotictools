import os

def open(device, **kwargs):
    return 'FACTS("Sunrise","Sunrise_Open","","0","");'

def close(device, **kwargs):
    return 'FACTS("Sunrise","Sunrise_Close","","0","");'


def measure(device, output_path=None, shake_duration=0, shake_settle_time=0, **kwargs):
    if not os.path.isabs(output_path):
	    output_path = os.path.abspath(output_path)

    plate_out = 0
    esc = '\x1b'
    wstr = 'FACTS("Sunrise","Sunrise_Measure","'+output_path
    wstr += ',MeasurementName:(SUNRISE'+esc+'2 Measurement 1'+esc
    wstr += '1ExtraDataFileHeadLine:'+esc+'1ExtraDataFileFootLine:'+esc
    wstr += '1ReadMode:Absorbance'+esc+'1MovePlateOutAfterMeasurement:'
    wstr += str(plate_out)+"'"+esc+'1SpinCDAfterMeasurement:-9999'+esc
    wstr += '1UserComment:'+esc+'1ExtraWarning:'+esc+'1DataRowWise:-9999'+esc
    wstr += '1DataAcquisitionMode:'+esc+'1DataSpeedOptimization:0'+esc
    wstr += '1ReadBarCode:-9999'+esc+'1GainPlatesMode:'+esc
    wstr += '1ExcitationFilter:-9999'+esc+'1EmissionFilter:-9999'+esc
    wstr += '1ExcitationFltWaveLength:-9999'+esc+'1EmissionFltWaveLength:-9999'+esc
    wstr += '1MeasurementFilter:620'+esc+'1ReferenceFilter:0'+esc
    wstr += '1ExcitationBandWidthMode:'+esc+'1ExcitationBandWidth:-9999'+esc
    wstr += '1EmissionBandWidthMode:'+esc+'1EmissionBandWidth:-9999'+esc
    wstr += '1AbsorbanceBandWidthMode:'+esc+'1AbsorbanceBandWidth:-9999'+esc
    wstr += '1FilterSwitchMode:'+esc+'1MirrorTypeMode:'+esc
    wstr += '1MirrorType:'+esc+'1AttenuationFltMode:'+esc
    wstr += '1AttenuationFltLevel:'+esc+'1AttenuationFltWellX:-9999'+esc
    wstr += '1AttenuationFltWellY:-9999'+esc+'1AbsorbanceReadMode:Normal'+esc
    wstr += '1GainMode:1Gain:-9999'+esc+'1GainWellX:-9999'+esc
    wstr += '1GainWellY:-9999'+esc+'1GainLumiMode:'+esc+'1GainLumi:-9999'+esc
    wstr += '1AttenuationLumiMode:'+esc+'1IntegrationLagTime:-9999'+esc
    wstr += '1IntegrationTime:-9999'+esc+'1NumberOfFlashes:8'+esc
    wstr += '1IntegrationDelay:-9999'+esc+'1NumberOfDark:-9999'+esc
    wstr += '1DelayMoveFlashFluorescence:-9999'+esc+'1DelayMoveFlashAbsorbance:-9999'+esc
    wstr += '1DelayMoveFlashLuminescence:-9999'+esc+'1DelayMoveFlashFlt:-9999'+esc
    wstr += '1IntegrationLumi:-9999'+esc+'1Integration2Lumi:-9999'+esc
    wstr += '1IntegrationLumiMode:'+esc+'1IntegrationAbsorbance:-9999'+esc
    wstr += '1IntegrationAbsorbanceMode:'+esc+'1FltSyncRate:-9999'+esc
    wstr += '1FltAcquisitionTime:-9999'+esc+'1ShakeMode:Inside'+esc
    wstr += '1ShakeIntensity:Normal'+esc+'1ShakeDuration:'
    wstr += str(shake_duration)+esc+'1ShakeSettleTime:'+str(shake_settle_time)+esc
    wstr += '1ShakeModeBetween:Inside'+esc+'1ShakeIntensityBetween:High'+esc
    wstr += '1ShakeDurationBetween:0'+esc+'1ShakeSettleTimeBetween:0'+esc
    wstr += '1KineticNumber:0'+esc+'1KineticInterval:0'+esc+'1KineticPrepareMode:Normal'+esc
    wstr += '1StopWellMode:Off'+esc+'1StopWellWellX:1'+esc+'1StopWellWellY:1'+esc
    wstr += '1StopWellValue:2147483647'+esc+'1StartTempMode:'+esc
    wstr += '1StartTempValue:-9999'+esc+'1StartTemp2Value:-9999'+esc+'1StartLampMode:'+esc
    wstr += '1StartDelayMode:Off'+esc+'1StartDelayValue:0'+esc+'1ZPositionMode:'+esc
    wstr += '1ZPositionValue:-9999'+esc+'1ZPositionWellX:-9999'+esc
    wstr += '1ZPositionWellY:-9999'+esc+'1WaitTextExtended:'+esc
    wstr += '1WavelengthScanUseMode:'+esc+'1WavelengthScanType:Fixed'+esc
    wstr += '1DelayBetweenColors:-9999'+esc+'1WavelengthExStart:-9999'+esc
    wstr += '1WavelengthExEnd:-9999'+esc+'1WavelengthExNumber:-9999'+esc
    wstr += '1WavelengthEmStart:-9999'+esc+'1WavelengthEmEnd:-9999'+esc
    wstr += '1WavelengthEmNumber:-9999'+esc+'1WavelengthAbStart:-9999'+esc
    wstr += '1WavelengthAbEnd:-9999'+esc+'1WavelengthAbNumber:-9999'+esc
    wstr += '1WavelengthSpectrumType:-9999'+esc+'1WavelengthReferenceMeasurement:-9999'+esc
    wstr += '1WavelengthReferenceWellX:-9999'+esc+'1WavelengthReferenceWellY:-9999'+esc
    wstr += '1SpfName:1SpfComment:'+esc+'1SpfCDSpinProfile:'+esc+'1PartOfPlate:0'+esc
    wstr += '1FromWellX:1'+esc+'1FromWellY:1'+esc+'1ToWellX:12'+esc+'1ToWellY:8'+esc
    wstr += '1ReadsPerWellNumber:1'+esc+'1ReadsPerWellDistance:440'+esc
    wstr += '1ReadsPerWellPattern:Square'+esc+'1WithPlateCover:0'+esc
    wstr += '1UseCarrierPosition:-9999'+esc+'1ReadDirection:'+esc+'1PdfName:'+esc
    wstr += '1PdfComment:'+esc+'1PdfPlateType:MTP'+esc+'1PdfStartX:0'+esc+'1PdfStartY:0'+esc
    wstr += '1PdfEndX:99000'+esc+'1PdfEndY:63000'+esc+'1PdfNumberX:12'+esc
    wstr += '1PdfNumberY:8'+esc+'1PdfWellForm:Round'+esc+'1PdfWellDiameter:-2'+esc
    wstr += '1PdfWellDepth:10000'+esc+'1PdfPlateHeight:15000'+esc
    wstr += '1PdfCoverHeight:20000'+esc+'1PdfSkirtHeight:0'+esc
    wstr += '1PdfMoveSpeed:0'+esc+'1PdfCDGrid:'+esc+'1PdfCDWellTable:'+esc
    wstr += '1SkipTopmostPlate:-9999'+esc+'1RestackAfterLastPlate:-9999'+esc
    wstr += '1","0","");'
    return wstr

if __name__ == '__main__':
    print measure('test', 'qweoi', '12i')

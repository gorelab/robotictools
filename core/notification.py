"""
Holds a few classes for a simple notification system.
"""
class Observable(object):
    def __init__(self):
        self._observers = []

    def _add_observer(self, observer):
        """
        observer : must contain an update method
        """
        self._observers.append(observer)

    def _notify(self, *args, **kwargs):
        """
        passes the parameters to the observers
        """
        for o in self._observers:
            o.update(self, *args, **kwargs)

class Observer(object):
    def update(self, observable, *args, **kwargs):
        print '{} {} {}'.format(observable, args, kwargs)


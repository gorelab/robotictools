"""
Manages the script commands.

TODO: Implement online mode.
"""
import warnings

class Scripter(object):
    def __init__(self, mode):
        """
        Parameters
        -----------
        mode : ['offline' | 'online-simulation' | 'online-real']
            'online-simulation' : issues the commands to a real robot.
            'online-real' : issues commands to a virtual robot.
            offline : use to produce an .esc file that can later be loaded with the robot.
        """
        self.log = []
        self.controller = None
        self.mode = mode

    def login(self, mode='online-simulation'):
        self.mode = mode

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, mode='offline'):
        self._mode = mode

        mode_options = 'offline', 'online-simulation', 'online-real'

        if mode not in (mode_options):
            raise ValueError('mode must be one of the following: {}'.format(mode_options))

        if mode == 'offline':
            if self.controller is not None:
                self.logout()
                self.controller = None
        else:
            import drivers.evoware.freedom_evoware_controller as controller
            self.controller = controller.EVOSoftwareController('Admin', 'admin')
            print('Switching to online mode')
            warnings.warn("""Before running a script in online mode, make sure that it works\
            as a generated esc file. The reason is that the EVOware software may be unable\
            to catch some errors like pipetting too much liquid into a tip, so
            you could break the LiHa tips.""")
            if mode == 'online-simulation':
                self.controller.login('simulation')
            elif mode == 'online-real':
                self.controller.login('real')

    def logout(self):
        """ Only relevant if using online mode. """
        print('Switching to offline mode.')
        self.controller.logout()
        self.controller = None
       
    def initialize(self):
        self.controller.initialize()

    def update(self, *args, **kwargs):
        command = kwargs['command']
        self.log.append(command)
        if self.mode != 'offline':
            self.controller.execute_command(command)

    def load_workspace(self, esc_file):
        logged_in = self.controller is not None

        if not logged_in:
            self.login()

        self.controller.load_workspace(esc_file)

        if not logged_in:
            self.logout()

    def wait(self, time, units='sec'):
        """
        time : amount to wait
        units: 'sec' | 'min' | 'hr'

        This is just temporarily here.
        """
        if units == 'sec':
            time = time
        elif units == 'min':
            time = time * 60
        elif units == 'hr':
            time = time * 3600
        timer_num = 1
        self.update(command='StartTimer("{}");'.format(timer_num))
        self.update(command='WaitTimer("{}", {});'.format(timer_num, time))

    def save(self, output_filename, mode='w'):
        """
        Saves the script to a file.

        Parameters
        ----------

        output_filename : str
            Path of file to save the script
        """
        with open(output_filename, mode) as f:
            script = '\n'.join(self.log) + '\n'
            f.write(script)
